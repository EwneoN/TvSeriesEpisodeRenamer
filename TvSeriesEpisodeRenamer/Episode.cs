﻿using System.IO;

namespace TvSeriesEpisodeRenamer {
  public class Episode
  {
    public string Name { get; set; }
    public string NewName { get; set; }
    public decimal Number { get; set; }
    public string Tag { get; set; }
    public FileInfo File { get; set; }
    public FileInfo NewFile { get; set; }
  }
}