﻿using System.Text.RegularExpressions;
using HtmlAgilityPack;

namespace TvSeriesEpisodeRenamer
{
  public class EpisodeNode
  {
    public string EpisodeName { get; }
    public decimal Number { get; }
    public string Tag { get; }

    public EpisodeNode(HtmlNode node)
    {
      HtmlNode nameNode = node.SelectSingleNode("td[@class='summary']/a") ?? 
                          node.SelectSingleNode("td[@class='summary']") ??
                          node.SelectSingleNode("td[2]");
      HtmlNode numberNode = node.SelectSingleNode("td[1]");

      EpisodeName = nameNode.InnerText;

      if (decimal.TryParse(numberNode.InnerText, out decimal number))
      {
        Number = number;
      }
      else
      {
        Number = decimal.Parse(Regex.Match(numberNode.InnerText, @"\d+").Value);
        Tag = Regex.Match(numberNode.InnerText, @"[^\d+]").Value;
      }
    }
  }
}