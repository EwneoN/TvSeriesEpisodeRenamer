﻿using CommandLine;

namespace TvSeriesEpisodeRenamer
{
  public class Options
  {
    [Option('s', "series", Required = true,
      HelpText = "The series you are wanting to get episode names for")]
    public string SeriesName { get; set; }

    [Option('d', "dir",
      HelpText = "The directory where the series is located.\n" +
                 "If not provided the current directory will be used")]
    public string SeriesDirectory { get; set; }

    [OptionArray('n', "seasons", 
      HelpText = "The season numbers you wish to soley run this process for",
      DefaultValue = new double[0])]
    public double[] SeasonNumbers { get; set; }

    [OptionArray('f', "s-formats",
      HelpText = "The different formats the season folders are expected to be expressed as regular expressions.\n" +
                 "This provides a way to handle complex season folder names.",
      DefaultValue = new [] { @"(?<=s(eason)?\s?(-|_|\.|,)?\s?)\d+(\.\d+)?" })]
    public string[] SeasonFolderFormats { get; set; }

    [OptionArray('t', "types",
      HelpText = "The different file types the file episodes are expected to be.\n" +
                 "This provides a way to filter out not video file types.\n" +
                 "Defaults to a list of all common video file types eg mp4, mkv, avi, webm, mov, flv", 
      DefaultValue = new [] { "mp4", "mkv", "avi", "webm", "mov", "flv", "m4v" })]
    public string[] FileTypes { get; set; }

    [OptionArray('e', "e-formats",
      HelpText = "The different formats the episode file names are expected to be expressed as regular expressions.\n" +
                 "This provides a way to handle complex season folder names.",
      DefaultValue = new[] { @"(?<=e(pisode)\s?(-|_|\.|,)?\s?)\d+(\.\d+)?", @"(?<=.+\[\d+\.)\d+(\.\d+)?(?=\])" })]
    public string[] EpisodeFileFormats { get; set; }

    [Option('i', "number", DefaultValue = true,
      HelpText = "Flag to indicate whether or not to include episode number in the new filename.\n" +
                 "Only relevant if episode has an actual name.\n" +
                 "Defaults to true.")]
    public bool IncludeEpisodeNoInFileName { get; set; }

    [Option('v', "verbose", DefaultValue = true,
      HelpText = "Flag to indicate whether or not to write to console.")]
    public bool Verbose { get; set; }
  }
}