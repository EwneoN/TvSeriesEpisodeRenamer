﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;

namespace TvSeriesEpisodeRenamer
{
  public static class Program
  {
    public static void Main(string[] args)
    {
      MainAsync(args).GetAwaiter().GetResult();
    }

    private static async Task MainAsync(string[] args)
    {
      Options options = new Options();
      HelpText.AutoBuild(options);
      Parser.Default.ParseArgumentsStrict(args, options, () => throw new ArgumentException());

      if (options.Verbose)
      {
        Trace.Listeners.Add(new ConsoleTraceListener());
      }

      SeriesRenamer renamer = new SeriesRenamer(options);

      await renamer.RenameEpisodes();
    }
  }
}