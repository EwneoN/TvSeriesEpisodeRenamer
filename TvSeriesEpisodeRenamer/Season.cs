﻿using System.Collections.Generic;
using System.IO;

namespace TvSeriesEpisodeRenamer
{
  public class Season
  {
    public string Name { get; set; }
    public decimal Number { get; set; }
    public DirectoryInfo Directory { get; set; }
    public List<Episode> Episodes { get; set; }
  }
}