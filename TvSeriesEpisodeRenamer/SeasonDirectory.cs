﻿using System.IO;

namespace TvSeriesEpisodeRenamer
{
  public class SeasonDirectory
  {
    public string Name { get; set; }
    public decimal Number { get; set; }
    public DirectoryInfo Directory { get; set; }
  }
}