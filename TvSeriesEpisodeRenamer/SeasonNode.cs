﻿using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;

namespace TvSeriesEpisodeRenamer
{
  public class SeasonNode
  {
    public string SeasonName { get; }
    public decimal Number { get; }

    public List<EpisodeNode> EpisodeNodes { get; }

    public SeasonNode(string name, decimal number)
    {
      SeasonName = name;
      Number = number;

      EpisodeNodes = new List<EpisodeNode>();
    }

    public SeasonNode(string name, decimal number, HtmlNode episodeTableNode)
    {
      SeasonName = name;
      Number = number;

      HtmlNodeCollection episodeRows = episodeTableNode.SelectNodes("tr[@class='vevent']");

      if (episodeRows != null)
      {
        EpisodeNodes = episodeRows
          .Where(r => r.SelectNodes("td").Count > 1)
          .Select(r => new EpisodeNode(r)).ToList();

        return;
      }

      EpisodeNodes = new List<EpisodeNode>();

      HtmlNode titleNode = episodeTableNode.SelectSingleNode("tr[@style='color:white;text-align:center']") ??
                           episodeTableNode.SelectSingleNode("tr[@style='color:black;text-align:center']");
      HtmlNodeCollection justBeforeCells = episodeTableNode.SelectNodes("tr/td[contains(@style, 'border-bottom:3px solid')]");

      HtmlNode firstEpNode = titleNode.NextSibling;

      while (firstEpNode.Name != "tr")
      {
        firstEpNode = firstEpNode.NextSibling;
      }

      EpisodeNodes.Add(new EpisodeNode(firstEpNode));

      foreach (HtmlNode justBeforeCell in justBeforeCells)
      {
        HtmlNode row = justBeforeCell.ParentNode;

        HtmlNode nextEpNode = row.NextSibling;

        while (nextEpNode.Name != "tr")
        {
          nextEpNode = nextEpNode.NextSibling;

          if (nextEpNode == null)
          {
            break;
          }
        }

        if (nextEpNode != null)
        {
          EpisodeNodes.Add(new EpisodeNode(nextEpNode));
        }
      }
    }
  }
}