﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TvSeriesEpisodeRenamer
{
  public class Series
  {
    public string Name { get; set; }
    public DirectoryInfo Directory { get; set; }
    public List<Season> Seasons { get; set; }
  }
}
