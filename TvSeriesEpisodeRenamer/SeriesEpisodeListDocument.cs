﻿using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;

namespace TvSeriesEpisodeRenamer
{
  public class SeriesEpisodeListDocument : HtmlDocument
  {
    public string Name { get; }
    public decimal[] SeasonNumbers { get; }
    public List<SeasonNode> SeasonNodes { get; }

    public SeriesEpisodeListDocument(string name, decimal[] seasonNumbers)
    {
      Name = name;
      SeasonNumbers = seasonNumbers;
      SeasonNodes = new List<SeasonNode>();
    }

    public new void LoadHtml(string html)
    {
      base.LoadHtml(html);

      HtmlNode overviewNode = GetElementbyId("Series_overview").ParentNode;
      HtmlNode seasonTableNode = overviewNode.NextSibling;

      while (seasonTableNode.Name != "table")
      {
        seasonTableNode = seasonTableNode.NextSibling;
      }

      HtmlNodeCollection seasonTableRows = seasonTableNode.SelectNodes("tr[not(@style)]");
      seasonTableRows.RemoveAt(0);

      foreach (HtmlNode seasonTableRow in seasonTableRows.Where(r => (r.SelectNodes("td")?.Count ?? 0) > 4))
      {
        SeasonNode node = ExtractSeasonNode(seasonTableRow);

        if (node.EpisodeNodes.Any())
        {
          SeasonNodes.Add(node);
        }
      }
    }

    private SeasonNode ExtractSeasonNode(HtmlNode seasonTableRow)
    {
      HtmlNode numberNode = seasonTableRow.SelectSingleNode("td[2]/a");

      string numberText = numberNode.InnerText;
      string seasonNodeId = numberNode.Attributes["href"].Value.Split('#')[1];

      HtmlNode seasonNode = GetElementbyId(seasonNodeId);
      HtmlNode episodeTableNode = seasonNode.ParentNode.NextSibling;

      while (episodeTableNode.Name != "table")
      {
        episodeTableNode = episodeTableNode.NextSibling;
      }

      decimal number = decimal.Parse(numberText);

      return !SeasonNumbers.Any() || SeasonNumbers.Contains(number) 
        ? new SeasonNode(seasonNode.InnerText, number, episodeTableNode)
        : new SeasonNode(seasonNode.InnerText, number);
    }
  }
}