﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TvSeriesEpisodeRenamer
{
  public class SeriesRenamer
  {
    private readonly Options _Options;

    public SeriesRenamer(Options options)
    {
      _Options = options;
    }

    public async Task RenameEpisodes()
    {
      DirectoryInfo seriesDir = new DirectoryInfo(string.IsNullOrWhiteSpace(_Options.SeriesDirectory)
        ? Environment.CurrentDirectory
        : _Options.SeriesDirectory);

      List<SeasonDirectory> seaonDirs = seriesDir.GetDirectories()
                                                 .Where(dir =>
                                                   _Options.SeasonFolderFormats.Any(f => IsSeasonDir(dir.Name, f)))
                                                 .Select(dir => new SeasonDirectory
                                                 {
                                                   Name = dir.Name,
                                                   Number = GetSeasonNumber(dir.Name, _Options.SeasonFolderFormats),
                                                   Directory = dir
                                                 })
                                                 .Where(IsRelevantSeason)
                                                 .ToList();

      Series series = new Series
      {
        Name = _Options.SeriesName,
        Directory = seriesDir,
        Seasons = seaonDirs.Select(MakeSeason).ToList()
      };

      string response;

      using (HttpClient client = new HttpClient())
      {
        string urlSeriesName = series.Name.Replace(" ", "_");
        response = await client.GetStringAsync($"https://en.wikipedia.org/wiki/List_of_{urlSeriesName}_episodes");
      }

      SeriesEpisodeListDocument document = new SeriesEpisodeListDocument(series.Name, 
        _Options.SeasonNumbers.Select(n => (decimal)n).ToArray());
      document.LoadHtml(response);

      foreach (Season season in series.Seasons)
      {
        SeasonNode seasonNode = document.SeasonNodes.FirstOrDefault(n => n.Number == season.Number);

        if (seasonNode == null)
        {
          continue;
        }

        foreach (Episode episode in season.Episodes)
        {
          EpisodeNode episodeNode = seasonNode.EpisodeNodes
            .FirstOrDefault(n => (n.Number == episode.Number || season.Number * 100 + n.Number == episode.Number) && 
                                 n.Tag == episode.Tag);

          if (episodeNode == null)
          {
            continue;
          }

          string episodeName = episodeNode.EpisodeName
            .Replace("?", ".")
            .Replace("!", ".")
            .Replace(":", ",")
            .Replace("\"", "")
            .Replace("'", "")
            .Replace("/", "-")
            .Replace("\n", "-");

          episode.NewName = _Options.IncludeEpisodeNoInFileName
            ? $"Episode {episode.Number} - {episodeName}{episode.File.Extension}"
            : $"{episodeName}{episode.File.Extension}";

          string newPath = Path.Combine(season.Directory.FullName, episode.NewName);

          File.Move(episode.File.FullName, newPath);

          episode.NewFile = season.Directory.GetFiles(episode.NewName).FirstOrDefault();
        }
      }
    }

    private bool IsRelevantSeason(SeasonDirectory directory)
    {
      return directory.Number >= 0 &&
             (!_Options.SeasonNumbers.Any() ||
             _Options.SeasonNumbers.Any(n => directory.Number == (decimal)n));
    }

    private Season MakeSeason(SeasonDirectory directory)
    {
      return new Season
      {
        Name = directory.Name,
        Number = directory.Number,
        Directory = directory.Directory,
        Episodes = directory.Directory.GetFiles()
                      .Where(f => _Options.FileTypes.Any(t => IsEpisodeFile(f, t)))
                      .Select(MakeEpisode)
                      .Where(e => e.Number >= 0)
                      .ToList()
      };
    }

    private Episode MakeEpisode(FileInfo file)
    {
      string episodeFileName = Path.GetFileNameWithoutExtension(file.FullName);

      return new Episode
      {
        Name = episodeFileName,
        File = file,
        Number = GetEpisodeNumber(episodeFileName, _Options.EpisodeFileFormats)
      };
    }

    private static bool IsSeasonDir(string directoryName, string directoryFormat)
    {
      return Regex.IsMatch(directoryName, directoryFormat, RegexOptions.IgnoreCase);
    }

    private static bool IsEpisodeFile(FileSystemInfo file, string fileType)
    {
      return file.Extension.Substring(1) == fileType;
    }

    private static decimal GetSeasonNumber(string directoryName, string[] directoryFormats)
    {
      foreach (string directoryFormat in directoryFormats)
      {
        Match match = Regex.Match(directoryName, directoryFormat, RegexOptions.IgnoreCase);

        if (match.Success)
        {
          return decimal.Parse(match.Value);
        }
      }

      return -1;
    }

    private static decimal GetEpisodeNumber(string fileName, string[] fileFormats)
    {
      foreach (string fileFormat in fileFormats)
      {
        Match match = Regex.Match(fileName, fileFormat, RegexOptions.IgnoreCase);

        if (match.Success)
        {
          return decimal.Parse(match.Value);
        }
      }

      return -1;
    }
  }
}